<?php

function queue_admin_settings_form() {
  $form = array();
  $form['header message'] = array(
    '#type' => 'markup',
    '#markup' => 'Save not implemented yet. Read-only page at the moment (bonus: you can\'t break anything from here).',
  );
  $mob_queue = module_exists('mob_queue');

  $headers = array(
    t('Name'),
    t('Class'),
    t('Items'),
    t('Time'),
    array('data' => t('Skip on cron'), 'colspan' => '2'),
  );

  if ($mob_queue) {
    $header_link_options = array(
      'query' => array('destination' => current_path()),
    );
    $headers[] = l(t('DQH'), 'admin/config/system/mob_queue', $header_link_options);
  }

  $headers = array_merge($headers, array(
    array('data' => t('Status'), 'colspan' => '2'),
    t('Configure'),
  ));

  $form['table'] = array(
    '#type' => 'table',
    '#header' => $headers,
  );

  $defined_queue_backends = queue_admin_defined_queue_backends();

  $defined_queues       = queue_admin_defined_queues(TRUE);
  $defined_queues_alter = queue_admin_defined_queues(FALSE);

  foreach ($defined_queues as $queue_name => $queue_info) {
    $queue = DrupalQueue::get($queue_name);
    $queue_info['time'] = isset($queue_info['time']) ? 
      $queue_info['time'] : t('unset');

    $queue_info['skip on cron'] = isset($queue_info['skip on cron']) ?
      $queue_info['skip on cron'] : FALSE;

    $is_enabled = TRUE;
    if (!isset($defined_queues_alter[$queue_name])) {
      $is_enabled = FALSE;
      $is_skipped = TRUE;
    } else {
      $defined_queues_alter[$queue_name]['skip on cron'] =
        (isset($defined_queues_alter[$queue_name]['skip on cron'])) ?
          $defined_queues_alter[$queue_name]['skip on cron'] : FALSE;

      $is_skipped = $defined_queues_alter[$queue_name]['skip on cron'];
    }

    $form['table'][$queue_name]['name'] = array(
      '#type' => 'markup',
      '#markup' => $queue_name,
    );

    $form['table'][$queue_name]['class'] = array(
        '#type' => 'select',
        '#options' => array_combine($defined_queue_backends, $defined_queue_backends),
        '#default_value' => get_class($queue),
    );

    $form['table'][$queue_name]['items'] = array(
      '#type' => 'markup',
      '#markup' => number_format($queue->numberOfItems()),
    );

    $form['table'][$queue_name]['time'] = array(
      '#type' => 'markup',
      '#markup' => $queue_info['time'],
    );

    // The amount of link columns should be identical to the 'colspan'
    // attribute in #header above.
    $form['table'][$queue_name]['skip on cron'] = array(
      '#type' => 'checkbox',
      '#attributes' => array('title' => t('Is the queue skipped on cron runs?')),
      '#disabled' => TRUE,
      '#value' => $is_skipped,
    );

    $form['table'][$queue_name]['skip on cron enable'] = array(
      '#type' => 'checkbox',
      '#attributes' => array('title' => t('Check to skip queue on cron runs.')),
      '#disabled' => FALSE,
      '#value' => variable_get('queue_admin_' . $queue_name . '_skip_on_cron', FALSE),
    );

    if ($mob_queue) {
      $form['table'][$queue_name]['mob'] = array(
        '#type' => 'checkbox',
        '#disabled' => FALSE,
        '#value' => variable_get('mob_queue_' . $queue_name, FALSE),
      );
    }

    $form['table'][$queue_name]['status'] = array(
      '#type' => 'checkbox',
      '#attributes' => array('title' => t('Is the queue enabled?')),
      '#disabled' => TRUE,
      '#value' => $is_enabled,
    );
    $form['table'][$queue_name]['disable'] = array(
      '#type' => 'checkbox',
      '#attributes' => array('title' => t('Check to disable queue')),
      '#disabled' => FALSE,
      '#value' => variable_get('queue_admin_' . $queue_name . '_disable', FALSE),
    );

    $configure = array();
    if (isset($queue_info['configure link'])) {
      $configure = array(
        '#type' => 'link',
        '#title' => t('Configure'),
        '#href' => queue_admin_link($queue_name . '/configure'),
        //'#href' => $configure_link['href'],
        '#options' => array(
          'attributes' => array(
            'class' => array('module-link', 'module-link-configure'),
            'title' => $configure_link['description'],
        )),
      );
    }
    $form['table'][$queue_name]['configure link'] = $configure;
  }

  //return system_settings_form($form);
  return $form;
}

function queue_admin_link($link) {
   return 'admin/config/queue_admin/' . $link;
}

function queue_admin_defined_queues($skip_alter = FALSE) {
  // Invoke hook_queue_info().
  $queues = module_invoke_all('cron_queue_info');
  if (!$skip_alter) {
    drupal_alter('cron_queue_info', $queues);
  }
  return $queues;
}

function queue_admin_defined_queue_backends() {
  _queue_admin_module_load_all();
  $backends = array();
  foreach (get_declared_classes() as $className) {
    if (in_array('DrupalQueueInterface', class_implements($className))) {
      $backends[] = $className;
    }
  }
  return $backends;
}

function _queue_admin_module_load_all() {

  $system_get_info = system_get_info('module_enabled');

  foreach($system_get_info as $module_name => $info) {
    foreach ($info['files'] as $filename) {
      $pathinfo = pathinfo($filename);
      $ext = $pathinfo['extension'];
      if (strtolower($ext) === 'test') {
        continue;
      }
      $basename = $pathinfo['filename'];
      module_load_include($ext, $module_name, $basename);
    }
  }

}
